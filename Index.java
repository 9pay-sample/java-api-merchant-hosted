import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Index {
    private static final String MERCHANT_KEY = "Nrx9wW";

    private static final String MERCHANT_SECRET_KEY = "37eXAlepnGLz5tMdWTmTFtXbNWWIJgWMdHm";

    private static final String END_POINT = "https://sand-payment.9pay.vn";

    private static final String BASE_URL = "http://fcdcc4767acb.ngrok.io/";

    public static void main(String[] args) throws UnsupportedEncodingException {
        String time = String.valueOf(System.currentTimeMillis());
        String invoiceNo = randomInvoiceNo();
        String amount = "15000";
        String lang = "VN";
        String currency = "VND";
        String client_ip = "127.0.0.1";
        String method = "ATM_CARD";
        String card_number = "9704000000000018";
        String card_name = "NGUYEN VAN A";
        String expire_month = "03";
        String expire_year = "07";
        String cvc = "123";

        Map<String, String> map = new TreeMap<String, String>();
        map.put("merchantKey", MERCHANT_KEY);
        map.put("time", time);
        map.put("invoice_no", invoiceNo);
        map.put("amount", amount);
        map.put("description", "description");
        map.put("return_url", BASE_URL);
        map.put("lang", lang);
        map.put("currency", currency);
        map.put("client_ip", client_ip);
        map.put("method", method);
        map.put("card_number", card_number);
        map.put("card_name", card_name);
        map.put("expire_month", expire_month);
        map.put("expire_year", expire_year);
        map.put("cvc", cvc);
        String queryHttp = http_build_query(map);
        String message = buildMessage(queryHttp, time);
        String signature = signature(message,MERCHANT_SECRET_KEY);
        byte[] byteArray = jsonEncode(map).getBytes();
        String baseEncode = Base64.getEncoder().encodeToString(byteArray);

        //Map<String, String> queryBuild = new HashMap<String, String>();
        //queryBuild.put("baseEncode", baseEncode);
        //queryBuild.put("signature", signature);

        // Call 9PAY API
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(END_POINT + "/api/payments/create"))
                .header("Content-Type", "application/json")
                .header("Date", time)
                .header("Authorization", "Signature Algorithm=HS256,Credential=" + MERCHANT_KEY + ",SignedHeaders=,Signature=" + signature)
                .POST(HttpRequest.BodyPublishers.ofByteArray(byteArray))
                .build();

        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenAccept(System.out::println)
                .join();

    }

    public static String jsonEncode(Map<String, String> array) throws UnsupportedEncodingException {
        String string = "";
        Iterator it = array.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry) it.next();
            String key = entry.getKey();
            String value = entry.getValue();
            if (!it.hasNext()) {
                string = string + '"' + key + '"' + ":" + '"' + value + '"';
            } else {
                string = string + '"' + key + '"' + ":" + '"' + value + '"' + ",";
            }
        }
        return "{" + string + "}";
    }

    public static String buildMessage(String queryHttp, String time) {
        StringBuffer sb = new StringBuffer();
        sb.append("POST");
        sb.append("\n");
        sb.append(END_POINT + "/api/payments/create");
        sb.append("\n");
        sb.append(time);
        sb.append("\n");
        sb.append(queryHttp);
        return sb.toString();
    }

    public static String signature(String queryHttp, String secretKey) {
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmac = sha256_HMAC.doFinal(queryHttp.getBytes());
            return Base64.getEncoder().encodeToString(hmac);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String randomInvoiceNo() {
        int leftLimit = 48;
        int rightLimit = 122;
        int targetStringLength = 10;
        Random random = new Random();
        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97)).limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
        return generatedString;
    }

    public static String http_build_query(Map<String, String> array) {
        String reString = "";
        Iterator it = array.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry) it.next();
            String key = entry.getKey();
            String value = entry.getValue();
            reString += key + "=" + value + "&";
        }
        reString = reString.substring(0, reString.length() - 1);
        reString = java.net.URLEncoder.encode(reString);
        reString = reString.replace("%3D", "=").replace("%26", "&");
        return reString;
    }
}
